from django.contrib import admin
from django.urls import path, re_path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('', include('apps.autor.urls')),
    re_path('', include('apps.libro.urls')),
    re_path('', include('apps.prestamo.urls')),
]
