from pathlib import Path

from .base import *

# Build paths inside the project like this: BASE_DIR / 'subdir'.

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dbbilio',
        'USER': 'user-biblio',
        'PASSWORD': '1234',
        'PORT': '5432',
        'HOST': 'localhost',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
