from django.db import models
from apps.autor.models import Autor

# Create your models here.

class Lector(Autor):

    class Meta:
        verbose_name_plural = "Lectores"