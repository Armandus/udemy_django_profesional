from django.db import models
from apps.nacionalidad.models import Nacionalidad

# managers
from .manager import AutorManager

# Create your models here.

class Autor(models.Model):
    nombre = models.CharField(
            "Nombre",
            max_length=50
        );
    apellidos = models.CharField(
            "Apellidos",
            max_length=100
        );
    nacionalidad = models.ForeignKey(
            Nacionalidad,
            on_delete=models.CASCADE
        );
    edad = models.PositiveIntegerField(
            "Edad"
        );

    # manager
    objects = AutorManager()

    def __str__(self):
        return (self.nombre + " " + self.apellidos + 
            " [ " + self.nacionalidad.nombre + " ]" +
            " edad = " + str(self.edad))

    class Meta:
        verbose_name_plural = "Autores"


