from django.db import models



class AutorManager(models.Manager):
    """ manager para el modelo autor """

    def listar_autores(self):
        return self.all()

    def filtro_texto(self, texto):
        return self.filter(
            models.Q(nombre__icontains = texto) |
            models.Q(apellidos__icontains = texto)
        )
