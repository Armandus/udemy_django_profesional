from django.shortcuts import render
from django.views.generic import ListView
from .models import Autor

# Create your views here.

class ListaAutores(ListView):
    model = Autor
    context_object_name = "autores"
    template_name = "autor/lista.html"

    def get_queryset(self):
        filtro = self.request.GET.get('filtro')        
        return Autor.objects.filtro_texto(filtro)


