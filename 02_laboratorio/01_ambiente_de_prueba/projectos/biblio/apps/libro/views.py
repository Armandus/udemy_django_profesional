
from django.shortcuts import render
from django.views.generic import ListView
from .models import Libro

# Create your views here.

class ListaLibros(ListView):
    model = Libro
    context_object_name = "libros"
    template_name = "libro/lista.html"


    def get_queryset(self):
        filtro = self.request.GET.get('filtro')        
        fecha1 = self.request.GET.get('fecha1')        
        fecha2 = self.request.GET.get('fecha2')  
        
        if fecha1 is None or fecha2 is None:
            return Libro.objects.filtro_texto(filtro)
        else:
            return Libro.objects.filtro_fechas_trg(filtro, fecha1, fecha2)

