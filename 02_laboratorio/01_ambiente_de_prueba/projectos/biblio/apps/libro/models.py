from django.db import models
from apps.categoria.models import CategoriaLibro
from apps.autor.models import Autor
from django.db.models.signals import post_save

# apps tercero
from  PIL import Image

# Create your models here.
# managaer

from .manager import LibroManager

# Create your models here.

class Libro(models.Model):
    titulo = models.CharField(
            "Nombre",
            max_length=50
        );
    categoria_libro = models.ForeignKey(
            CategoriaLibro,
            on_delete=models.CASCADE
        );
    autores = models.ManyToManyField(
            Autor
        );
    fecha_lanzamiento = models.DateTimeField(
            "Fecha de Lanzamiento"
        );
    portada = models.ImageField(
            upload_to = "portadas"
        );
    visitas = models.PositiveIntegerField(
            "visitas",            
    )

    # manager
    objects = LibroManager()

    def __str__(self):
        return self.titulo + " [visitas] = " + str(self.visitas) + " - " + str(self.fecha_lanzamiento)

    class Meta:
        verbose_name_plural = "Libros"


def optimize_image(sender, instance, **kwargs):
    if instance.portada:
        portada = Image.open(instance.portada.path)
        portada.save(instance.portada.path, quality=20, optimize=True)

post_save.connect(optimize_image, sender=Libro)