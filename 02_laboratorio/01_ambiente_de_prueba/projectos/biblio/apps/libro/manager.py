import datetime

from django.db import models
from django.contrib.postgres.search import TrigramSimilarity
from django.contrib.postgres.operations import UnaccentExtension 

def log(mens, texto):
    print("#"*50)
    print(mens, " --- ", texto)

class LibroManager(models.Manager):
    """ manager para el modelo libro """

    def listar_libros(self):
        return self.all()

    def filtro_texto(self, texto):
        if texto is None: 
            texto = ""
        log("texto = ", texto)

        if len(texto) < 3:
            log("if1 = ", texto)
            return self.filter(
                    models.Q(titulo__icontains = texto),
                )
            
        else:            
            log("if2 = ", texto)
            return self.filter(
                titulo__unaccent__trigram_similar = texto,
            )

    def filtro_fechas(self, filtro, fecha1, fecha2):
        
        if fecha1 == "" and fecha2 == "":
            return self.filtro_texto(filtro)
        
        if fecha1 != "" and fecha2 == "":
            dF1 = datetime.datetime.strptime(fecha1, '%Y-%m-%d')        
            return self.filter(
                models.Q(titulo__icontains = filtro),
                models.Q(fecha_lanzamiento__gt = dF1),
            )
        if fecha1 == "" and fecha2 != "":
            dF2 = datetime.datetime.strptime(fecha2, '%Y-%m-%d')        
            return self.filter(
                models.Q(titulo__icontains = filtro),
                models.Q(fecha_lanzamiento__lt = dF2),
            )
        
        dF1 = datetime.datetime.strptime(fecha1, '%Y-%m-%d')        
        dF2 = datetime.datetime.strptime(fecha2, '%Y-%m-%d')        

        return self.filter(
            models.Q(titulo__icontains = filtro),
             models.Q(fecha_lanzamiento__range = [dF1, dF2]),
        )

    def filtro_fechas_trg(self, filtro, fecha1, fecha2):
        
        if fecha1 == "" and fecha2 == "":
            return self.filtro_texto(filtro)
        
        if fecha1 != "" and fecha2 == "":
            dF1 = datetime.datetime.strptime(fecha1, '%Y-%m-%d')        
            return self.filter(
                models.Q(titulo__icontains = filtro),
                models.Q(fecha_lanzamiento__gt = dF1),
            )
        if fecha1 == "" and fecha2 != "":
            dF2 = datetime.datetime.strptime(fecha2, '%Y-%m-%d')        
            return self.filter(
                models.Q(titulo__icontains = filtro),
                models.Q(fecha_lanzamiento__lt = dF2),
            )
        
        dF1 = datetime.datetime.strptime(fecha1, '%Y-%m-%d')        
        dF2 = datetime.datetime.strptime(fecha2, '%Y-%m-%d')        

        return self.filter(
            models.Q(titulo__trigram_similar = filtro),
             models.Q(fecha_lanzamiento__range = [dF1, dF2]),
        )
