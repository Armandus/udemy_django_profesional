from django.db import models

# Create your models here.
from apps.libro.models import Libro
from apps.lector.models import Lector
from .signals import fnEjecutarSeñales


class Prestamo(models.Model):
    lector = models.ForeignKey(
            Lector,
            on_delete=models.CASCADE
        );
    libro = models.ForeignKey(
            Libro,
            on_delete=models.CASCADE
        );
    fecha_prestamo = models.DateTimeField(
            "Fecha de Prestamo",
            blank = True,
            null = True,

        );
    fecha_devolucion = models.DateTimeField(
            "Fecha de Devolucion",
            blank = True,
            null = True,
        );

    devuelto = models.BooleanField(
            "devuleto"
        );

    def __str__(self):
        return str(self.lector) + " [" + str(self.libro) + "] " + str(self.devuelto)

    def save(self, *arg, **karg):

        print("titulo del libreo = ", self.libro.titulo)
        super(Prestamo, self).save(*arg, **karg)

    class Meta:
        verbose_name_plural = "Prestamos"

    

fnEjecutarSeñales(Prestamo)