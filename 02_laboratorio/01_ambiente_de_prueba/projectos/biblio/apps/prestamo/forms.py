
from .models import Prestamo
from apps.libro.models import Libro
# Create your views here.

from django.forms import ModelForm
from django import forms


class PrestamoForm(ModelForm):        
    class Meta:
        model = Prestamo
        fields = [
            'libro',
            'lector'
        ]
        
class MultiplePrestamoForm(ModelForm): 

    libros = forms.ModelMultipleChoiceField(
        queryset = Libro.objects.all(),
        required = True,
        widget = forms.CheckboxSelectMultiple, 
        )

    
    def getLibros(self):
        return(self.libros)

    class Meta:
        model = Prestamo
        fields = [            
            'lector',
            'libros'                        
        ]
        

        