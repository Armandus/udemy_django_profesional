import datetime

from django.shortcuts import render
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect

from .models import Prestamo
from .forms import PrestamoForm, MultiplePrestamoForm

class PrestamoFormView(FormView):
    template_name = "prestamo/add.html"
    form_class = PrestamoForm
    success_url = '.'

    def form_valid(self, form):

        hoy = datetime.date.today()
        print("#"*50, hoy)
        Prestamo.objects.create(
            lector = form.cleaned_data['lector'],
            libro = form.cleaned_data['libro'],
            fecha_prestamo = hoy,
            devuelto = False            
        )
        
        return super(PrestamoFormView, self).form_valid(form)


class AddPrestamo(FormView):
    template_name = "prestamo/add.html"
    form_class = PrestamoForm
    success_url = '.'

    def form_valid(self, form):

        hoy = datetime.date.today()

        obj, created = Prestamo.objects.get_or_create(
            lector = form.cleaned_data['lector'], 
            libro = form.cleaned_data['libro'], 
            devuelto = False,
            defaults = {
                'fecha_prestamo' : hoy
            }
        )

        print("#"*40, "created = ", created )
        if created:
            return super(AddPrestamo, self).form_valid(form)
        else:
            HttpResponseRedirect("/")


class AddMultiplePrestamo(FormView):
    template_name = "prestamo/multiple-add.html"
    form_class = MultiplePrestamoForm
    success_url = '.'

    
    def form_valid(self, form):
        lstPrestamo = []
        for libro in form.cleaned_data["libros"]:
            prestamo = Prestamo(
                lector = form.cleaned_data['lector'],
                libro = libro,
                fecha_prestamo = datetime.datetime.today(),
                devuelto = False
            )
            lstPrestamo.append(prestamo)
        
        Prestamo.objects.bulk_create(lstPrestamo)
        
        return super(AddMultiplePrestamo, self).form_valid(form)
        

