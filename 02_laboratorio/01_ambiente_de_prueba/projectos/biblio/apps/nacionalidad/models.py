from django.db import models

# Create your models here.


class Nacionalidad(models.Model):
    nombre = models.CharField("nombre",max_length=30)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Nacionalidedes"