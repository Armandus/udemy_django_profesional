from django.db import models

# Create your models here.
class CategoriaLibro(models.Model):
    nombre = models.CharField("categoria_libro",max_length=50)

    def __str__(self):
        return self.nombre


    class Meta:
        verbose_name_plural = "Categorias"