from django.db import models
import os
directorio = os.getcwd()
print("#"*30, directorio)
# hola
from app01.models import Departamento

from ckeditor.fields import RichTextField

# Create your models here.

class Habilidades(models.Model):
    habilidades = models.CharField('Habilidad', max_length=50)

    class Meta:
        verbose_name = "habilidad"
        verbose_name_plural = "habilidades de empleados"

    def __str__(self):
        return str(self.id) + " - " + self.habilidades


class Empleado(models.Model):
    """ Modelo para tabla empleado"""
    
    JOB_CHOICES = (
            ('0', "Contador"    ),
            ('1', "Administrador"   ),
            ('3', "Economista"  ),
            ('4', "Otro"    ),
        )
    
    first_name = models.CharField('Nombre', max_length=60)
    last_name = models.CharField('Apellidos', max_length=60)
    full_name = models.CharField('Nombre Completo', max_length=120, blank=True)
    job = models.CharField('Trabajo', max_length=1, choices=JOB_CHOICES)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to = "empleado", blank=True, null = True)
    habilidades = models.ManyToManyField(Habilidades)
    hoja_vid = RichTextField()
    
    def __str__(self):
        strJob = "no hay"
        for eleccion in self.JOB_CHOICES:
            if eleccion[0] == self.job:
                strJob = eleccion[1]
        #return str(self.first_name) + " " + str(self.last_name) + " [" + str(strJob) + "] - " +  str(self.departamento.short_name)
        return "= " + str(self.id) + " " + str(self.full_name) + " [" + str(strJob) + "] - " +  str(self.departamento.short_name)
        