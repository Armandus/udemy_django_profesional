from django.contrib import admin
from django.urls import path, re_path, include

from . import views

# uso de  nombre de dominio
app_name = "persona_app"

urlpatterns = [
    path('', views.InicioView.as_view(), name= "home"),
    path(
            'listar-todo-empleados/', 
        views.ListAllEmpleados.as_view(),
        name = "empleados_todos"
        
        ),
    path(
            'empleados-admin/', 
        views.ListAllEmpleadosAdmin.as_view(),
        name = "empleados_admin"
        
        ),
    path('listar-por-area/<short_name>/', views.ListAllByAreaEmpleado.as_view()),
    path('buscar-empleado/', views.ListaEmpleadoByKword.as_view()),
    path('habilidades/<empleado>', views.ListaHabilidadesByEmpleado.as_view()),
    path(
        'ver-empleado/<pk>/', 
        views.EmleadoDetailView.as_view(),
        name = "empleado_por_id"
        ),
    path('add-empleado/', views.EmpleadoCreateView.as_view(), name = "agregar_empleado"),
    path('success/', views.SuccessView.as_view(), name = "correcto"),
    path('update-empleado/<pk>/', views.EmpleadoUpdateView.as_view(), name = "modificar_empleado"),
    path('delete-empleado/<pk>/', views.EmpleadoDeleteView.as_view(), name = "borrar_empleado"),
	
]
