import pdb

from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse_lazy

from django.views.generic import (
                            ListView, 
                            DetailView,
                            CreateView,
                            TemplateView,
                            UpdateView,
                            DeleteView)


# Create your views here.

from .models import Empleado

class ListAllEmpleados(ListView):
    template_name = 'app02/lista_all.html'
    paginate_by = 4
    #ordering = 'first_name'
    context_object_name = 'empleados'

    def get_queryset(self): 
        palabra_clave = self.request.GET.get("kword")        
        
        if palabra_clave != "" and palabra_clave is not None :
            lista = Empleado.objects.filter(
                Q(first_name__icontains = palabra_clave) |
                Q(last_name__icontains = palabra_clave) |
                Q(departamento__name__icontains = palabra_clave) 
            ).order_by('first_name')
        else:
            lista = Empleado.objects.all().order_by('first_name')
        return lista
    
class ListAllEmpleadosAdmin(ListView):
    template_name = 'app02/lista_admin.html'
    paginate_by = 10
    ordering = 'first_name'
    context_object_name = 'empleados'
    model = Empleado

    
class ListAllByAreaEmpleado(ListView):
    """ Lista Empleados de un área """
    template_name = 'app02/lista_area.html'

    def get_queryset(self):
        area = self.kwargs['short_name']
        lista = Empleado.objects.filter(
        departamento__short_name = area
        )
        return lista
    

class ListaEmpleadoByKword(ListView):
    template_name = "app02/by_kword.html"
    paginate_by = 4
    context_object_name = 'empleados'

    def get_queryset(self): 
        palabra_clave = self.request.GET.get("kword")
        if palabra_clave != "":
            lista = Empleado.objects.filter(
                first_name = palabra_clave
            )
        else:
            lista = Empleado.objects.all()
        return lista
        


class ListaHabilidadesByEmpleado(ListView):
    template_name = "app02/habilidades.html"    
    context_object_name = 'habilidades'

    def get_queryset(self): 
        id = self.kwargs['empleado']
        empleado = Empleado.objects.get(id=id)
        lista = [empleado, empleado.habilidades.all()]
        return lista
        
class EmleadoDetailView(DetailView):
    model = Empleado
    template_name = "app02/empleado_detail.html"

    def get_context_data(self, **kwargs):
        context = super(EmleadoDetailView, self).get_context_data(**kwargs)
        context['titulo'] = "Empleado del mes"
        return context

class SuccessView(TemplateView):
    template_name = "app02/success.html"

class EmpleadoCreateView(CreateView):
    model = Empleado
    template_name = "app02/add.html"    
    fields = [  "first_name",
                "last_name",
                "job",
                "avatar",
                "departamento",
                "habilidades"]    
    success_url = reverse_lazy("persona_app:empleados_admin")

    def form_valid(self, form):        
        empleado = form.save(commit=False)
        empleado.full_name = empleado.first_name + " " + empleado.last_name
        empleado.save()
        return super(EmpleadoCreateView, self).form_valid(form)

    
class EmpleadoUpdateView(UpdateView):
    model = Empleado
    template_name = "app02/update.html"
    fields = [
        'id',
        'first_name',
        'last_name',
        'job',
        'avatar',
        'departamento',
        'habilidades',
    ]
    success_url = reverse_lazy("persona_app:empleados_admin")

    def form_valid(self, form):        
        print("*"*40, "form_valid")
        empleado = form.save(commit=False)
        empleado.full_name = empleado.first_name + " " + empleado.last_name
        empleado.save()
        return super(EmpleadoUpdateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        print("*"*40, "POST")
        self.object = self.get_object()
        print(request.POST)
        print(request.POST['last_name'])
        request.POST._mutable = True
        request.POST['last_name'] = "mutable"
        request.POST._mutable = False
        return super().post(request, *args, **kwargs)
        

class EmpleadoDeleteView(DeleteView):
    model = Empleado
    template_name = "app02/delete.html"
    success_url = reverse_lazy("persona_app:empleados_admin")


class InicioView(TemplateView):
    template_name = 'inicio.html'