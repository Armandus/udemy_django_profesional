import pdb
from django.shortcuts import render
from django.views.generic.edit import FormView
from .forms import NewDepartamentoForm
from app02.models import Empleado
from app01.models import Departamento

class NewDepartamentoFormView(FormView):
	template_name = "app01/new_departamento.html"
	form_class = NewDepartamentoForm
	success_url = "/"

	def form_valid(self, form):
		print("#"*40, "Dentro del Dapartamento")

		# Departamento
		departamento = form.cleaned_data["departamento"]
		shortname = form.cleaned_data["corto"]
		depa = Departamento(
			name = departamento,
			short_name = shortname
		)
		depa.save()

		# Empleado
		nombre = form.cleaned_data["nombre"]
		apellidos = form.cleaned_data["apellidos"]

		Empleado.objects.create(
			first_name = nombre,
			last_name = apellidos,
			job = '1',
			departamento = depa,			
		)

		

		return super(NewDepartamentoFormView, self).form_valid(form)

