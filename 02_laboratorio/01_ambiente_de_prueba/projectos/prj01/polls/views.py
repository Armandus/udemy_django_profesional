from django.http import (
        HttpResponse, 
        Http404,
        HttpResponseRedirect
)

from django.shortcuts import render, get_object_or_404

from django.urls import reverse

from django.db.models import F

from django.views import generic

from django.utils import timezone

from .models import (
                Question as Q, 
                Choice as C
)

# Create your views here.

def index(request):    
    print(" # "*30, timezone.now())
    last_question = Q.objects.filter(pub_date__lte = timezone.now()).order_by('-pub_date')[:5]    
    context = {'latest_question_list' : last_question}
    return render(request, 'polls/index.html', context)    

def detail(request, question_id):    
    q = get_object_or_404(Q, pk =question_id)    
    context = {'question':q}
    return render(request, 'polls/detail.html', context)    

def results(request, question_id):
    q = get_object_or_404(Q, pk=question_id)
    context = {'question':q}
    return render(request, 'polls/results.html', context)

def vote(request, question_id):
    q = get_object_or_404(Q, pk=question_id)
    try:
        selected_choice = q.choice_set.get(pk=request.POST['choice'])
    except(keyError, C.DoesNotExist):
        context = {
                'question':q, 
                'error_message': 'You didnt select a choice'
        }
        return render(request, 'polls/detail.html', context)
    else:
        selected_choice.votes = F('votes') + 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(q.id,)))


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'
    
    def get_queryset(self):        
        return Q.objects.filter(pub_date__lte = timezone.now()).order_by('-pub_date')[:5]
        
        
    

class DetailView(generic.DetailView):
    model = Q
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model = Q
    template_name = 'polls/results.html'