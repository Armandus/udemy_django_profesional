import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models  import Question as Q

def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Q.objects.create(question_text=question_text, 
        pub_date=time)

class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'],[])


class QuestionModelTests(TestCase):
    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        f_q = Q(pub_date = time)
        self.assertIs(f_q.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False for questions 
        whose pub_date is older than 1 day
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Q(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() returns True for questions whose
        pub_date is within the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, 
            seconds=59)
        recent_question = Q(pub_date = time)
        self.assertIs(recent_question.was_published_recently(), True)

