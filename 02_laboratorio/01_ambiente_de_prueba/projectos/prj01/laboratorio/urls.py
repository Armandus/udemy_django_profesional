from django.contrib import admin
from django.urls import path, re_path, include
from . import views


urlpatterns = [    	
	path('laboratorio-crear-prueba/', views.PruebaCreateView.as_view()),
	path('laboratorio-grid-fundation/', views.GridFundationTemplateView.as_view()),
	path('laboratorio-home/', views.HomeTemplateView.as_view()),
]
