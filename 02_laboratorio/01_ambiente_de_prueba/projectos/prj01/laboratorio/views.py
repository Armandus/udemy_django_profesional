from django.shortcuts import render
from django.views.generic import CreateView
from django.views.generic import TemplateView
from . import models
from . import forms 

# Create your views here.

class PruebaCreateView(CreateView):
    template_name = "laboratorio/prueba-create.html"
    model = models.Prueba    
    form_class = forms.PruebaForm
    success_url = "/"

class GridFundationTemplateView(TemplateView):
    template_name = "laboratorio/grid_template.html"

class HomeTemplateView(TemplateView):
    template_name = "laboratorio/home.html"


class MovilTemplateView(TemplateView):
    template_name = "laboratorio/movil.html"