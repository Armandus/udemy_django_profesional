from django import forms
from . import models

class PruebaForm(forms.ModelForm):

    class Meta:
        model = models.Prueba
        fields = ('titulo',
                 'subtitulo',
                 'cantidad'
        )
        widgets = {
            'titulo': forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese texto aquí'
                }
            )
        }

    def clean_cantidad(self):
        cantidad = self.cleaned_data["cantidad"]
        if cantidad < 10:
            raise forms.ValidationError('cantidad menor a 10')
        
        return cantidad
