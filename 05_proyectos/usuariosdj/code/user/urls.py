from django.contrib import admin
from django.urls import path
#
from .views import (
    UserCreateView, 
    LoginUserForm, 
    HomePage,
    UpdatePasswordView,
    CodeVerifacationView
)

app_name = "user"
urlpatterns = [
    path('user-add', UserCreateView.as_view(), name="add"),
    path('user-login', LoginUserForm.as_view(), name="login"),
    path('user-logout', LoginUserForm.as_view(), name="logout"),
    path('user-index', HomePage.as_view(), name="index"),
    path('user-pass', UpdatePasswordView.as_view(), name="pass"),
    path('user-verif/<pk>/', CodeVerifacationView.as_view(), name="verif"),
]
