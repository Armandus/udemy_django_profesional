from django.shortcuts import render
from django.core.mail import send_mail
from django.views.generic import CreateView, View, TemplateView

from django.http import HttpResponse, HttpResponseRedirect

from django.views.generic.edit import FormView
from django.urls import reverse, reverse_lazy
from django.contrib.auth import  authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import User
from .forms import (
    UserModelForm, 
    LoginForm, 
    UpdatePasswordForm,
    VerificationForm
)


from .funtions import code_generator

class UserCreateView(FormView):
    
    form_class = UserModelForm
    template_name = "user/add.html"
    success_url = reverse_lazy('home:inicio')

    
    def form_valid(self, form):
        # Generamos el código
        codigo = code_generator()

        user = User.objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            nombre = form.cleaned_data['nombre'],
            apellidos = form.cleaned_data['apellidos'],
            genero = form.cleaned_data['genero'],
            codregistro = codigo
        )
        # enviar el codigo al email del user
        asunto = "Confirmación de Email"
        mensaje = "Código de verificación {}".format(codigo)
        email_remitente = 'scero.sistemas@gmail.com'

        #
        try:
            send_mail(asunto, mensaje, email_remitente, [form.cleaned_data['email'],])
        except Exception as e:
            print("#"*100, str(e))                    

        #return super(UserCreateView, self).form_valid(form)
        return (
            HttpResponseRedirect(reverse(
                                    "user:verif",
                                    kwargs = {'pk':user.id}
                                    )
                                )
            )


class LoginUserForm(FormView):
    form_class = LoginForm
    template_name = "user/login.html"
    success_url = reverse_lazy('home:inicio')

    def form_valid(self, form):
        
        username = form.cleaned_data["nombre_usuario"]
        password = form.cleaned_data["clave"]
        user = authenticate(            
            username = username,
            password = password,
        )  
        login(self.request, user)
        if user is not None:
            print("#", "Usuario Valido")            
        else:
            print("#", "Usuario Invalido")            
            

        return super(LoginUserForm, self).form_valid(form)


class LogoutView(View):
    def get(self, request, *args, **kargs):
        logout(request)
        return(
            HttpResponseRedirect(reverse("user:login"))
            )

class HomePage(LoginRequiredMixin, TemplateView):
    template_name = "home/index.html"
    login_url = reverse_lazy("user:login")


class UpdatePasswordView(LoginRequiredMixin, FormView):
    form_class = UpdatePasswordForm
    template_name = "user/update.html"
    login_url = reverse_lazy("user:login")
    success_url = reverse_lazy('user:login')

    def get_form_kwargs(self):
        kwargs = super(UpdatePasswordView,self).get_form_kwargs()
        kwargs.update({'username': self.request.user.username})
        return kwargs


class CodeVerifacationView(FormView):
    template_name = "user/verification.html"
    form_class = VerificationForm
    success_url = reverse_lazy('user:login')

    def get_form_kwargs(self):
        kwargs = super(CodeVerifacationView, self).get_form_kwargs()
        kwargs.update({
            'pk': self.kwargs['pk']
        })
        return kwargs

    def form_valid(self, form):
        #
        return super(CodeVerifacationView, self).form_valid(form)