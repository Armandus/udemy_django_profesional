from django.forms import ModelForm
from django import forms
from django.contrib.auth import authenticate, login

from .models import User

class UserModelForm(ModelForm):

    password1 = forms.CharField(
        label = "Contraseña",        
        widget = forms.PasswordInput(
            attrs = {
                'placeholder':'Contraseña'
            }
        )
    )
    password2 = forms.CharField(
        label = "Contraseña",
        widget = forms.PasswordInput(
            attrs = {
                'placeholder':'Repetir Contraseña'
            }
        )
    )


    class Meta:
        model = User
        fields = (
                    'username',
                    'email',
                    'nombre',
                    'apellidos',
                    'genero'
            )


    def clean_password2(self):        
        if (self.cleaned_data['password1'] 
            != self.cleaned_data['password2']):
            self.add_error('password2', 'Las contraseñas no son las mismas')


class LoginForm(forms.Form):
    nombre_usuario = forms.CharField(      
        label = "Nombre de Usuario",
        widget=forms.TextInput(
            attrs = {
                'placeholder' : 'Nombre de Usuario'
            }
        )        
    )
    clave = forms.CharField(      
        label = "Contraseña",
        widget=forms.PasswordInput(
            attrs = {
                'placeholder' : 'Contraseña'
            }
        )   
    )

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()        
        username = self.cleaned_data['nombre_usuario']
        password = self.cleaned_data['clave']
        
        if not authenticate(username = username,password = password,):
            raise forms.ValidationError("los datos de usuarios no son correctos")
        
        return self.cleaned_data

    def clean_password(self):        
        password = self.cleaned_data["clave"]
        if len(password) < 4:
            self.add_error("clave","el password no puede ser menor a 4 digitos")

class UpdatePasswordForm(forms.Form):
    password1 = forms.CharField(
        label = "Contraseña Anterior",
        widget = forms.PasswordInput(
            attrs = {
                'placeholder':'Contraseña Nueva'
            }
        )
    )
    password2 = forms.CharField(
        label = "Contraseña Nueva",
        widget = forms.PasswordInput(
            attrs = {
                'placeholder':'Repetir Contraseña Anterior'
            }
        )
    )

    def __init__(self, username, *args, **kwargs):
        self.username = username
        super(UpdatePasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(UpdatePasswordForm, self).clean()        
        pass1 = self.cleaned_data['password1']
        pass2 = self.cleaned_data['password2']
        nombre_usuario = self.username
        if not authenticate(username = nombre_usuario, password = pass1,):
            raise forms.ValidationError("los datos de usuarios no son correctos")
        else:
            user = User.objects.get(username=nombre_usuario)
            user.set_password(pass2)
            user.save()        
        return self.cleaned_data
    


class VerificationForm(forms.Form):
    codregistro = forms.CharField(label = "Código de Validación", max_length=50)

    def __init__(self, pk , *args, **kwargs):
        self.id_user = pk
        super(VerificationForm,self).__init__(*args, **kwargs)

    def clean_codregistro(self):       
        codigo = self.cleaned_data['codregistro']
        id = self.id_user
        si_activo = User.objects.cod_validation(id, codigo)
        if not si_activo:
            forms.ValidationError("Codigo Incorrecto")
        else:
            User.objects.activa_usuario(id)

            print("#"*50, "Se activo el Usiario {}".format(id))

    
    
    