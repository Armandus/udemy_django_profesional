from django.contrib import admin
from django.urls import path
from .views import HomeTemplateView, TemplatePruebaMixin

app_name = "home"
urlpatterns = [
    path('home/', HomeTemplateView.as_view(), name = "inicio" ),
    path('mixin/', TemplatePruebaMixin.as_view(), name = "mixin" ),
   
]
