from django.db import models
from django.conf import settings

from model_utils.models import TimeStampedModel
from ckeditor_uploader.fields import RichTextUploadingField
from .manager import EntryManager

class Category(TimeStampedModel):
    """ Categoría de entrada """

    short_name = models.CharField(
        'Nombre corto', 
        max_length=15,
        unique = True
    )
    name = models.CharField(
        'Nombre', 
        max_length=100
    )

    class Meta:
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'

    def __str__(self):
        return self.name


class Tag(TimeStampedModel):
    """ Etiquetas de un artículo """
    name = models.CharField(
        'Nombre', 
        max_length=30
    )

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return self.name


class Entry(TimeStampedModel):
    """ Modelo para entradas o artículos """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )    
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE
    )  
    tag =  models.ManyToManyField(Tag)
    title = models.CharField(
        'Titulo', 
        max_length=200
    )
    resume = models.TextField('Resumen')
    content = RichTextUploadingField()
    image = models.ImageField(
        'Imagen', 
        upload_to='Entry',
    )
    portada = models.BooleanField(default=False)
    in_home = models.BooleanField(default=False)
    public = models.BooleanField(default=True)
    slug = models.SlugField(editable=False, max_length=300)

    objects = EntryManager()

    class Meta:
        verbose_name = 'Entrada'
        verbose_name_plural = 'Entradas'

    def __str__(self):
        return self.title + " [{}]".format(self.public)
