from django.db import models



class EntryManager(models.Manager):
    """ procedimiento para entrada """

    def entrada_en_portada(self):
        return self.filter(
            public = True,
            portada = True,
        ).order_by('-created').first()

    def entradas_en_portada(self):
        return self.filter(
            public = True,
            in_home = True,
        ).order_by('-created')[1:6]

    def ultimas_entradas(self):
        return self.filter(
            public = True, 
            in_home = False,           
        ).order_by('-created')[:6]

    def buscar_entrada(self, kword, categoria):
        
        if len(categoria) > 0:            
            resultado = self.filter(
                    category__short_name=categoria,
                    title__icontains=kword,                    
                    public=True
                ).order_by('-created')            
        else:            
            resultado = self.filter(             
                    models.Q(title__icontains=kword)
                    | models.Q(resume__icontains=kword) ,
                    public=True
                ).order_by('-created')            
        
        return resultado