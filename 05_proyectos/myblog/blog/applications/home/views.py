import datetime
#
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse

from django.views.generic import (
    TemplateView,
    CreateView
)
# apps
from applications.entrada.models import Entry
from applications.home.models import Home

# forms
from . import forms

class HomePageView(TemplateView):
    template_name = "home/index.html"

    
    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        # contexto de portada
        context["portada"] = Entry.objects.entrada_en_portada()
        context["portadas"] = Entry.objects.entradas_en_portada()
        context["ultimas_entradas"] = Entry.objects.ultimas_entradas()
        context['home'] = Home.objects.latest('created')
        context['form'] = forms.SuscribersForm

        """ con = context["portadas"]
        import pdb
        pdb.set_trace() """
        return context
    


class SuscritorCreateViewCreateView(CreateView):
    form_class = forms.SuscribersForm
    success_url = '.'


class ContactoCreateView(CreateView):
    form_class = forms.ContactoForm
    success_url = '.'


