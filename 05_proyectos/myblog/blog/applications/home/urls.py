#
from django.urls import path
from . import views

app_name = "home_app"

urlpatterns = [
    path(
        '', 
        views.HomePageView.as_view(),
        name='plantilla',
    ),  
    path(
        'registro-suscripcion', 
        views.SuscritorCreateViewCreateView.as_view(),
        name='add-suscriptor',
    ),  
    path(
        'contacto', 
        views.ContactoCreateView.as_view(),
        name='add-contacto',
    ),
]